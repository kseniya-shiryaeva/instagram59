<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaseController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        if($this->getUser()){
            return $this->redirectToRoute('my_profile');
        }
        return $this->render('@App/Base/index.html.twig', array(
            // ...
        ));
    }

}
