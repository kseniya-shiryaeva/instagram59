<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/users")
 */
class UserController extends Controller
{
    /**
     * @Route("/{id}/follow", requirements={"id": "\d+"}, name="follow_user")
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function followAction(int $id)
    {
        $currentUser = $this->getUser();

        $foll = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        $em = $this->getDoctrine()->getManager();

        if(!$foll->hasFollower($currentUser)){

            $foll->addFollower($currentUser);
            $em->flush();
        }

        return $this->redirectToRoute("my_profile");
    }

}
