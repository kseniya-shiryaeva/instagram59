<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/posts")
 */

class PostController extends Controller
{
    /**
     * @Route("/", name="tape")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();


        return $this->render('@App/Post/index.html.twig', array(
            'posts'=>$posts,
            'user'=>$this->getUser()
        ));
    }

    /**
     * @Route("/my_profile", name="my_profile")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(Request $request, EntityManagerInterface $em)
    {
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();

        $form = $this->createForm(PostType::class, new Post(), array('method' => 'POST'));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $post = $form->getData();
            $post->setAuthor($this->getUser());

            $em->persist($post);
            $em->flush($post);

            return $this->redirectToRoute("my_profile");
        }

        return $this->render('@App/Post/my_profile.html.twig', array(
            'form'=> $form->createView(),
            'posts'=>$posts,
            'user'=>$this->getUser()
        ));
    }

    /**
     * @Route("/{id}/like", requirements={"id": "\d+"}, name="like_post")
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function likeAction(int $id)
    {
        $currentUser = $this->getUser();

        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

        $em = $this->getDoctrine()->getManager();

        if(!$post->hasLiker($currentUser)){

            $post->addLiker($currentUser);
            $em->flush();
        }

        return $this->redirectToRoute("my_profile");
    }


}
