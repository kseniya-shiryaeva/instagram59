<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     */
    private $posts;

    /**
     * @return ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="likers")
     */
    private $likedPosts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User")
     */
    private $followers;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        $this->followers = new ArrayCollection();
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }



    /**
     * @param Post $post
     */
    public function removeLikedPost(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addLikedPost(Post $post)
    {
        $this->likedPosts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param User $user
     */
    public function addFollower(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasFollower(User $user)
    {
        return $this->followers->contains($user);
    }

}
