<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends Fixture
{
    public const USER_ONE = 'user_one';
    public const USER_TWO = 'user_two';
    public const USER_THREE = 'user_three';

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('user_one')
            ->setEmail('user_one@mail.ru')
            ->setEnabled('1')
            ->setPlainPassword('123')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user);


        $user1 = new User();
        $user1
            ->setUsername('user_two')
            ->setEmail('user_two@mail.ru')
            ->setEnabled('1')
            ->setPlainPassword('123')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user1);

        $user2 = new User();
        $user2
            ->setUsername('user_three')
            ->setEmail('user_three@mail.ru')
            ->setEnabled('1')
            ->setPlainPassword('123')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user2);

        $manager->flush();

        $this->addReference(self::USER_ONE, $user);
        $this->addReference(self::USER_TWO, $user1);
        $this->addReference(self::USER_THREE, $user2);
    }
}
