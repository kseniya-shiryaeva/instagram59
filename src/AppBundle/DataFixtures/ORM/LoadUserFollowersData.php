<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserFollowersData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = $this->getReference(LoadUserData::USER_ONE);
        $follower = $this->getReference(LoadUserData::USER_TWO);
        $user
            ->addFollower($follower);

        $manager->persist($user);

        $user = $this->getReference(LoadUserData::USER_TWO);
        $follower = $this->getReference(LoadUserData::USER_THREE);
        $user
            ->addFollower($follower);

        $manager->persist($user);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
        );
    }

}
