<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserPostData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $post = new Post();
        /** @var User $author */
        $author = $this->getReference(LoadUserData::USER_ONE);
        /** @var User $liker */
        $liker = $this->getReference(LoadUserData::USER_TWO);
        $post
            ->setAuthor($author)
            ->setImage('5b0a95764407b.png')
            ->addLiker($liker);

        $manager->persist($post);

        $post = new Post();
        $author = $this->getReference(LoadUserData::USER_ONE);
        $post
            ->setAuthor($author)
            ->setImage('5b0a95764407b.png');

        $manager->persist($post);

        $post = new Post();
        $author = $this->getReference(LoadUserData::USER_TWO);
        /** @var User $liker1 */
        $liker1 = $this->getReference(LoadUserData::USER_ONE);
        /** @var User $liker2 */
        $liker2 = $this->getReference(LoadUserData::USER_THREE);
        $post
            ->setAuthor($author)
            ->setImage('5b0a95764407b.png')
            ->addLiker($liker1);
        $post->addLiker($liker2);

        $manager->persist($post);

        $post = new Post();
        $author = $this->getReference(LoadUserData::USER_TWO);
        $post
            ->setAuthor($author)
            ->setImage('5b0a95764407b.png')
            ->addLiker($liker);

        $manager->persist($post);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
        );
    }
}
